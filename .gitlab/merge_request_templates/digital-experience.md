**Reviewer Checklist:**
- [ ] I, the Assignee, have run [Axe tools](https://chrome.google.com/webstore/detail/axe-devtools-web-accessib/lhdoppojpmngadmnindnejefpokejbdd) on any updated pages, and fixed the relevant accessibility issues
- [ ] These changes meet a specific OKR or item in our Quarterly Plan.
- [ ] These changes work on both Safari and Chrome.
- [ ] These changes have been reviewed for Visual Quality Assurance and Functional Quality Assurance on Mobile, Desktop, and Tablet.
- [ ] These changes work with our Google Analytics and SEO tools.
- [ ] These changes have been documented as expected.
